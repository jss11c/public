#!/bin/bash
# don't change these
red='\E[31m'
green='\E[32m'
blue='\E[37m'
#
# 
# 
#
# It create a hidden director (.testing) in the assignment folder to keep the output
# of each run and the diff results. If you want to manually review the 
# results you can look in the folder.
#
TESTDIR=~/cop4530/hw3/.testing       # Location output and diff files
HISPATH=~/cop4530/hw3                 # Location of area51/ binaries
MYPATH=~/cop4530/hw3                  # Location of your binaries 
FILEPATH=~/cop4530/hw3/.files     # Location of your com files
WAIT=3									# Number in seconds for how long you want output to pause
HISBINFILES="wordbench_i.x"
MYBINFILES="wb.x"
SETTYPES="UOList UOVector"
#SETTYPES="UOList MOList UOVector MOVector"
#COMFILES="com.1 com.3"
COMFILES="com.1 com.2 com.3 com.4 com.5 com.6"
#
# Feel free to change this color to something that looks better on your screen
# Leave it blank to use your default. Choices are { $red, $green, $blue, "" } you can add others if you want
export color=$red
#
# Function to reset screen color to default
colorReset() 
{
  tput sgr0
}
#
# Pause
function pause()
{
  echo -en $color
  echo -e "\n\tPress Enter to Continue ..."
  tput sgr0
  read
}
#
#
#
# Testing if file directory exists, copy it over if not. If that fails, exit.
if [ ! -d $FILEPATH ] ; then
  echo -e "\n$FILEPATH does not exist."
  echo -ne "\tCreating $FILEPATH and files into it ... "
  if cp -r /home/majors/jscott/public/cop4530-hw3-files $FILEPATH ; then
    echo -e "Success"
  else
    echo -e "Failed"
    echo -e "Edit the \$FILEPATH variable in the script and try again"
    exit 1
  fi
fi
#
# If the testing directory doesn't exist, create it first.
if [ ! -d $TESTDIR ] ; then      
  mkdir $TESTDIR
fi
#
# Test to see if executables are reachable
for file in $HISBINFILES ; do
  if [ ! -f $HISPATH/$file ] ; then
    echo -en "\nFailed to find: $HISPATH/$file\n\tFetching them from area51 ... "
    if cp ~cop4530p/spring14/area51/$file $HISPATH/$file ; then
      echo -e "Success"
      chmod u+x $HISPATH/$file
    else
      echo -e "Failed!\n Fix the variable \$HISPATH in the script and try again"
      exit 1
    fi
  fi
done
# If the make fails, report failure and exit script.
if ! make >> /dev/null 2>&1 ; then
  echo -e "make failed:\n\tRun make manually to find problem and try again.\n::Exiting Script::"
  exit 1
fi
#
# Test to see if your executable is reachable
for file in $MYBINFILES ; do
  if [ ! -f $MYPATH/$file ] ; then
    echo -e "\nFailed to find: $MYPATH/$file\n\tCheck this path and try again.\n"
    exit 1
  fi
done
pushd $FILEPATH >> /dev/null 2>&1
#
# Now we run through a series of tests 
#

for st in $SETTYPES ; do
#
# Create new header files
#
  cat $FILEPATH/$st/wordbench.h.lnk | grep -v '^.*typedef.*fsu::.*SetType.*$' > $FILEPATH/$st/wordbench.h
  sed -i "s/^.*SetType.*wordset_\;$/typedef fsu::$st < EntryType , PredicateType >    SetType; \n&/g" $FILEPATH/$st/wordbench.h
  for com in $COMFILES ; do
    pushd $FILEPATH/$st >> /dev/null 2>&1
    if ! make >> /dev/null 2>&1 ; then
      echo -e "make failed on: $TESTDIR/$st"
      echo -e "\tRun make manually to find problem and try again."
      echo -e "Exiting Script"
      exit 1
    fi
    popd >> /dev/null 2>&1
    if [ -f temp.out ] ; then rm -f temp.out ; fi
    $HISPATH/wordbench_i.x $FILEPATH/$com > $TESTDIR/his.stdout.$st.$com
    if [ -f temp.out ] ; then mv -f temp.out $TESTDIR/his.out.$st.$com ; fi
    $FILEPATH/$st/wb.x $FILEPATH/$com > $TESTDIR/mine.stdout.$st.$com
    if [ -f temp.out ] ; then mv -f temp.out  $TESTDIR/mine.out.$st.$com ; fi
    if [ -f $TESTDIR/his.stdout.$st.$com ] && [ -f $TESTDIR/mine.stdout.$st.$com ] ; then
      diff $TESTDIR/his.stdout.$st.$com $TESTDIR/mine.stdout.$st.$com >  $TESTDIR/diff.stdout.$st.$com
      diffstatus1=$?
    fi
    if [ -f $TESTDIR/his.out.$st.$com ] && [ -f $TESTDIR/mine.out.$st.$com ] ; then
      diff $TESTDIR/his.out.$st.$com $TESTDIR/mine.out.$st.$com  > $TESTDIR/diff.out.$st.$com
      diffstatus2=$?
    fi
    if [ -f $TESTDIR/diff.stdout.$st.$com ] ; then
      echo -en $color
      echo -e "\n\t BEGIN $TESTDIR/diff.stdout.$st.$com" 
      echo -e " =====================================================================================" 
      colorReset
      cat  $TESTDIR/diff.stdout.$st.$com
      echo -en $color
      echo -e "\n =====================================================================================" 
      echo -e "\t END $TESTDIR/diff.stdout.$st.$com "
      colorReset
      if (( $diffstatus1 )) ; then sleep $WAIT ; fi 
    else
      echo -e "File $TESTDIR/diff.stdout.$st.$com not found!"
    fi
    if [ -f $TESTDIR/diff.out.$st.$com ] ; then
      echo -en $color
      echo -e "\n\t BEGIN $TESTDIR/diff.out.$st.$com"
      echo -e " =====================================================================================" 
      colorReset
      cat  $TESTDIR/diff.out.$st.$com
      echo -en $color
      echo -e "\n =====================================================================================" 
      echo -e "\t END  $TESTDIR/diff.out.$st.$com  "
      colorReset
      if (( $diffstatus2 )) ; then sleep $WAIT ; fi
    fi
  done
done
popd >> /dev/null 2>&1

